import React from 'react';
import { Helmet } from 'react-helmet';

import NavBar from '../components/NavBar/navbar';

export default ({ top, children, tab }) => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <meta
          name="Description"
          content="Online portfolio for Jeff Barnhart, Senior Software Engineer"
        ></meta>
        <title>Jeff Barnhart Portfolio</title>
        <link rel="canonical" href="https://www.jeff-barnhart.dev" />
        <html lang="en" />
      </Helmet>
      <NavBar current={tab} />
      <div className="bg-info text-white">
        <div className="container container-fluid">{top}</div>
      </div>
      <div className="container container-fluid">{children}</div>
    </>
  );
};
