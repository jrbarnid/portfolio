import React from 'react';
import { Helmet } from 'react-helmet';

import NavBar from '../components/NavBar/navbar';

export default ({ children, tab }) => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <meta
          name="Description"
          content="Online portfolio for Jeff Barnhart, Senior Software Engineer"
        ></meta>
        <title>Jeff Barnhart Portfolio</title>
        <link rel="canonical" href="https://www.jeff-barnhart.dev" />
        <html lang="en" />
      </Helmet>
      <NavBar current={tab} />
      <div className="container container-fluid py-5">{children}</div>
    </>
  );
};
