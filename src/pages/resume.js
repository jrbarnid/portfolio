import React from 'react';
import { Link, navigate } from 'gatsby';

import Layout from '../layouts/standard';
import Timeline from '../components/Timeline/timeline';

export default () => {
  return (
    <Layout tab="resume">
      <Timeline />
    </Layout>
  );
};
