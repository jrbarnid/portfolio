import React from 'react';

import ContactForm from '../components/ContactForm/contactForm';
import Layout from '../layouts/standard';

export default () => (
  <Layout tab="contact">
    <ContactForm />
  </Layout>
);
