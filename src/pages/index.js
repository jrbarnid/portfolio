import React from 'react';
import { Link, navigate } from 'gatsby';

import Card from '../components/Card/card';
import DockerLogo from '../../images/docker-vector-logo.svg';
import GatsbyLogo from '../../images/Gatsby-Logo.svg';
import GitlabLogo from '../../images/gitlab-icon-rgb.svg';
import Family from '../../images/barnhart_family.jpg';
import Layout from '../layouts/half';
import ReactLogo from '../../images/react.svg';
import VSCodeLogo from '../../images/visual-studio-code.svg';

export default function Home() {
  const _top = (
    <>
      <div className="row py-5">
        <div className="pt-3 col-9">
          <h3>Jeff Barnhart</h3>
          <h4>Senior Software Engineer at Lockheed Martin</h4>
          <br />
          <p>
            Experienced full stack web development software engineer
            with experience at multiple fortune 500 companies.
            <br />
            <br />I specialize in ReactJS and have production
            deployment experience in MERN and Jamstack. In fact,{' '}
            <Link to="https://gitlab.com/jrbarnid/portfolio">
              this website
            </Link>{' '}
            was built using jamstack tools!
          </p>
          {/* <span className="font-italic">Blogs coming soon</span> */}
          <div className="py-5 d-flex flex-column">
            <span className="font-italic small">
              Want to discuss job opportunities?
            </span>
            <div className="py-1">
              <Link to="/contact" className="btn btn-primary">
                Contact Me
              </Link>
            </div>
          </div>
        </div>
        <div>
          <img
            className="rounded-circle border-white"
            style={{ border: '4px solid' }}
            src={Family}
            alt="Barnhart Family"
            width={250}
            height={250}
          />
        </div>
      </div>
    </>
  );

  return (
    <Layout tab="home" top={_top}>
      <div>
        <div className="row pt-5">
          <h2>Favorite Tools</h2>
        </div>
        <div className="row py-3">
          <Card
            title="React"
            description="A JavaScript library for building user interfaces"
            link="https://reactjs.org"
            image={
              <div
                onClick={() => navigate('https://reactjs.org')}
                style={{ cursor: 'pointer' }}
              >
                <img
                  src={ReactLogo}
                  width="75"
                  height="75"
                  alt="react"
                />
              </div>
            }
            tags={['javascript', 'frontend']}
          />
          <Card
            title=""
            description="Blazing fast modern site generator for React. Go beyond static sites: build blogs, e-commerce sites, full-blown apps, and more with Gatsby."
            link="https://gatsbyjs.org"
            image={
              <div
                onClick={() => navigate('https://gatsbyjs.org')}
                style={{ cursor: 'pointer' }}
              >
                <img
                  src={GatsbyLogo}
                  width="200"
                  height="75"
                  alt="Gatsby"
                />
              </div>
            }
            tags={['javascript', 'frontend']}
          />
          <Card
            title="VS Code"
            description="A code editor redefined and optimized for building and debugging modern web and cloud applications."
            link="https://code.visualstudio.com"
            image={
              <div
                onClick={() =>
                  navigate('https://code.visualstudio.com')
                }
                style={{ cursor: 'pointer' }}
              >
                <img
                  src={VSCodeLogo}
                  width="75"
                  height="75"
                  alt="VS Code"
                />
              </div>
            }
            tags={['ide', 'code', 'devops']}
          />
          <Card
            title="GitLab"
            description="From project planning and source code management to CI/CD and monitoring, GitLab is a complete DevOps platform, delivered as a single application."
            link="https://www.gitlab.com"
            image={
              <div
                onClick={() => navigate('https://www.gitlab.com')}
                style={{ cursor: 'pointer' }}
              >
                <img
                  src={GitlabLogo}
                  width="75"
                  height="75"
                  alt="GitLab"
                />
              </div>
            }
            tags={['git', 'code', 'devops']}
          />
          <Card
            title="Docker"
            description="Package Software into Standardized Units for Development, Shipment and Deployment."
            link="https://www.docker.com"
            image={
              <div
                onClick={() => navigate('https://www.docker.com')}
                style={{ cursor: 'pointer' }}
              >
                <img
                  src={DockerLogo}
                  width="75"
                  height="75"
                  alt="Docker"
                />
              </div>
            }
            tags={['cloud', 'containers']}
          />
        </div>
      </div>
    </Layout>
  );
}
