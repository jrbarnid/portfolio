import React, { useState, useEffect } from 'react';
import { Link } from 'gatsby';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faHome,
  faFile,
  faEnvelope,
} from '@fortawesome/free-solid-svg-icons';
import {
  faLinkedin,
  faGitlab,
} from '@fortawesome/free-brands-svg-icons';

export default ({ current = 'home' }) => {
  const [expanded, setExpanded] = useState(false);
  const [collapsing, setCollapsing] = useState(false);

  useEffect(() => {
    setCollapsing(false);
  }, [expanded]);

  const srCurrent = (current, item) => {
    return (
      current === item && <span className="sr-only"> (current)</span>
    );
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="/">
        Jeff Barnhart
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarColor03"
        aria-controls="navbarColor03"
        aria-expanded="false"
        aria-label="Toggle navigation"
        onClick={() => {
          setCollapsing(true);
          setExpanded(!expanded);
        }}
      >
        <span className="navbar-toggler-icon btn-navbar"></span>
      </button>

      <div
        className={`collapse navbar-collapse ${
          expanded ? 'show' : ''
        } ${collapsing ? 'collapsing' : ''}`}
        id="navbarColor03"
      >
        <ul className="navbar-nav mr-auto">
          <li
            className={`nav-item ${
              current === 'home' ? 'active' : ''
            }`}
          >
            <Link className="nav-link" to="/">
              <FontAwesomeIcon icon={faHome} /> Home
              {srCurrent(current, 'home')}
            </Link>
          </li>
          <li
            className={`nav-item ${
              current === 'resume' ? 'active' : ''
            }`}
          >
            <Link className="nav-link" to="/resume">
              <FontAwesomeIcon icon={faFile} /> Resume
            </Link>
            {srCurrent(current, 'resume')}
          </li>
          <li
            className={`nav-item ${
              current === 'contact' ? 'active' : ''
            }`}
          >
            <Link className="nav-link" to="/contact">
              <FontAwesomeIcon icon={faEnvelope} /> Contact Me
            </Link>
            {srCurrent(current, 'contact')}
          </li>
          <li
            className={`nav-item ${
              current === 'code' ? 'active' : ''
            }`}
          >
            <Link
              className="nav-link"
              to="https://www.linkedin.com/in/jeff-barnhart-23a21014"
            >
              <FontAwesomeIcon icon={faLinkedin} /> LinkedIn
            </Link>
            {srCurrent(current, 'linkedIn')}
          </li>
          <li
            className={`nav-item ${
              current === 'linkedIn' ? 'active' : ''
            }`}
          >
            <Link
              className="nav-link"
              to="https://gitlab.com/jrbarnid/portfolio"
            >
              <FontAwesomeIcon icon={faGitlab} /> Code
            </Link>
            {srCurrent(current, 'code')}
          </li>
        </ul>
      </div>
    </nav>
  );
};
