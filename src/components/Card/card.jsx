import React from 'react';
import { Link } from 'gatsby';

export default ({ title, link, image, description, tags }) => {
  const _img = image || null;

  return (
    <div className="col-xl-4 col-sm-4 py-3">
      <div className="card shadow-sm">
        <div className="card-header">
          <div className="d-flex align-items-center">
            {_img}
            <h3 className="px-2">{title}</h3>
          </div>
        </div>
        <div className="card-body d-flex" style={{ height: '253px' }}>
          <div className="d-flex flex-column justify-content-between">
            <p>{description}</p>
            <Link to={link}>Learn more!</Link>
          </div>
        </div>
        <div className="card-footer">
          {tags.map((tag) => {
            return <div className="badge">{tag}</div>;
          })}
        </div>
      </div>
    </div>
  );
};
