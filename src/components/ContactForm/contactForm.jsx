import React from 'react';

import { useFormik } from 'formik';
import * as Yup from 'yup';

export default () => {
  const encode = (data) => {
    return Object.keys(data)
      .map(
        (key) =>
          encodeURIComponent(key) +
          '=' +
          encodeURIComponent(data[key]),
      )
      .join('&');
  };

  const formik = useFormik({
    initialValues: {
      'bot-field': '',
      'form-name': 'contact',
      firstName: '',
      lastName: '',
      email: '',
      comments: '',
    },
    validationSchema: Yup.object({
      firstName: Yup.string().required('First name is required'),
      lastName: Yup.string().required('Last name is required'),
      email: Yup.string()
        .email('Invalid email address')
        .required('Email address is required'),
      comments: Yup.string(),
    }),
    onSubmit: (values, actions) => {
      fetch('/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: encode({ 'form-name': 'contact', ...values }),
      })
        .then(() => {
          alert('Success');
          actions.resetForm();
        })
        .catch(() => {
          alert('Error');
        })
        .finally(() => actions.setSubmitting(false));
    },
  });

  return (
    <form
      name="contact"
      data-netlify="true"
      data-netlify-honeypot="bot-field"
      method="post"
      onSubmit={formik.handleSubmit}
    >
      <h3>Contact Me</h3>
      <input type="hidden" name="bot-field" />
      <input type="hidden" name="form-name" value="contact" />
      <div className="row py-3">
        <div className="col-lg col-md col-sm-12 py-1">
          <label htmlFor="firstName">First Name</label>
          <input
            id="firstName"
            name="firstName"
            type="text"
            className="form-control"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.firstName}
          />
          {formik.touched.firstName && formik.errors.firstName ? (
            <div className="text-danger">
              {formik.errors.firstName}
            </div>
          ) : null}
        </div>
        <div className="col-lg col-md col-sm-12 py-1">
          <label htmlFor="lastName">Last Name</label>
          <input
            id="lastName"
            name="lastName"
            type="text"
            className="form-control"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.lastName}
          />
          {formik.touched.lastName && formik.errors.lastName ? (
            <div className="text-danger">
              {formik.errors.lastName}
            </div>
          ) : null}
        </div>
      </div>
      <div className="row py-3">
        <div className="col-lg col-md col-sm-12">
          <label htmlFor="email">Email Address</label>
          <input
            id="email"
            name="email"
            type="email"
            className="form-control"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
          />
          {formik.touched.email && formik.errors.email ? (
            <div className="text-danger">{formik.errors.email}</div>
          ) : null}
        </div>
      </div>
      <div className="row py-3">
        <div className="col-lg col-md col-sm-12">
          <label htmlFor="comments">Comments</label>
          <textarea
            id="comments"
            name="comments"
            type="text"
            rows="4"
            className="form-control"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.comments}
          />
          {formik.touched.comments && formik.errors.comments ? (
            <div className="text-danger">
              {formik.errors.comments}
            </div>
          ) : null}
        </div>
      </div>
      <div className="py-3">
        <button
          type="submit"
          className="btn btn-primary"
          disabled={formik.isSubmitting}
        >
          Submit
        </button>
      </div>
    </form>
  );
};
