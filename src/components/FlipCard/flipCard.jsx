import React, { useState } from 'react';

import ReactCardFlip from 'react-card-flip';

import '../../styles/styles.css';

export default ({ first, second }) => {
  const [cardIsFlipped, setCardIsFlipped] = useState(false);

  return (
    <ReactCardFlip isFlipped={cardIsFlipped} infinite>
      <div
        className="d-flex flex-column justify-content-between"
        onClick={() => setCardIsFlipped(!cardIsFlipped)}
        style={{ cursor: 'pointer', minHeight: '232px' }}
      >
        {first}
        <div className="text-primary">View More Details</div>
      </div>

      <div
        onClick={() => setCardIsFlipped(!cardIsFlipped)}
        style={{ cursor: 'pointer', minHeight: '232px' }}
      >
        {second}
      </div>
    </ReactCardFlip>
  );
};
