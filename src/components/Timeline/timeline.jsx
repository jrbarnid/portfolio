import React from 'react';

import {
  VerticalTimeline,
  VerticalTimelineElement,
} from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faGraduationCap,
  faBriefcase,
} from '@fortawesome/free-solid-svg-icons';
import CardFlip from '../FlipCard/flipCard';

import '../../styles/styles.css';

export default () => {
  return (
    <VerticalTimeline>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="2019 - present"
        iconStyle={{
          background: 'rgb(33, 150, 243)',
          color: '#fff',
        }}
        icon={<FontAwesomeIcon icon={faBriefcase} size="lg" />}
      >
        <CardFlip
          first={
            <>
              <div>
                <h3 className="vertical-timeline-element-title">
                  Senior Software Engineer
                </h3>
                <h4 className="vertical-timeline-element-subtitle">
                  Lockheed Martin
                </h4>
                <h4 className="vertical-timeline-element-subtitle">
                  Tampa, FL
                </h4>
              </div>
              <p>
                Full stack web development
                <br />
                React | GatsbyJS | Docker | AWS | CI/CD
              </p>
            </>
          }
          second={
            <>
              <h3>Details</h3>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  Helped faciliate the adoption and creation of new
                  AI/ML tools via internal site (React, Gatsby,
                  Docker)
                </li>
                <li className="list-group-item">
                  Drove user discovery using multiple ElasticSearch
                  indices
                </li>
                <li className="list-group-item">
                  Maintained reliable CI/CD procedures with GitLab and
                  Docker Enterprise
                </li>
              </ul>
            </>
          }
        />
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="Feb 2019 - Sept 2019"
        iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
        icon={<FontAwesomeIcon icon={faBriefcase} size="lg" />}
      >
        <CardFlip
          first={
            <>
              <div>
                <h3 className="vertical-timeline-element-title">
                  Software Engineer
                </h3>
                <h4 className="vertical-timeline-element-subtitle">
                  Lockheed Martin
                </h4>
                <h4 className="vertical-timeline-element-subtitle">
                  Tampa, FL
                </h4>
              </div>
              <p>
                Full stack web development
                <br />
                React | Kotlin | Cloud Foundry | MySQL | CI/CD
              </p>
            </>
          }
          second={
            <>
              <h3>Details</h3>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  Drove price reductions across the org by
                  coordinating inventory purchases via website (React,
                  MySQL, PCF)
                </li>
                <li className="list-group-item">
                  Helped spread digital transformation by developing a
                  data literacy quiz
                </li>
                <li className="list-group-item">
                  Utilized blue/green deployments to minimize downtime
                </li>
              </ul>
            </>
          }
        />
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="2018 - 2019"
        iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
        icon={<FontAwesomeIcon icon={faBriefcase} size="lg" />}
      >
        <CardFlip
          first={
            <>
              <div>
                <h3 className="vertical-timeline-element-title">
                  Software Engineer
                </h3>
                <h4 className="vertical-timeline-element-subtitle">
                  BST Global
                </h4>
                <h4 className="vertical-timeline-element-subtitle">
                  Tampa, FL
                </h4>
              </div>
              <p>
                C# desktop application development
                <br />
                C# | .NET | T-SQL | WPF
              </p>
            </>
          }
          second={
            <>
              <h3>Details</h3>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  Designed solutions to meet scalable,
                  high-performance requirements
                </li>
                <li className="list-group-item">
                  Integrated applications with T-SQL and Concur APIs
                </li>
                <li className="list-group-item">
                  Improved user experience by optimizing SQL queries
                </li>
              </ul>
            </>
          }
        />
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="2016 - 2018"
        iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
        icon={<FontAwesomeIcon icon={faBriefcase} size="lg" />}
      >
        <CardFlip
          first={
            <>
              <div>
                <h3 className="vertical-timeline-element-title">
                  Associate Software Engineer
                </h3>
                <h4 className="vertical-timeline-element-subtitle">
                  Lockheed Martin
                </h4>
                <h4 className="vertical-timeline-element-subtitle">
                  Orlando, FL
                </h4>
              </div>
              <p>
                C++ Aircraft training simulation development
                <br />
                C++ | F-35 | C# | MySQL | Python
              </p>
            </>
          }
          second={
            <>
              <h3>Details</h3>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  Reduced wasted time by automating vendor imports
                  from XML to MySQL database
                </li>
                <li className="list-group-item">
                  Successfully delivered projects on aggressive
                  deadlines
                </li>
                <li className="list-group-item">
                  Helped communicate successes and roadblocks to
                  project managers and team leads
                </li>
              </ul>
            </>
          }
        />
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--school"
        date="Graduated 2016"
        iconStyle={{ background: 'rgb(245, 0, 87)', color: '#fff' }}
        icon={<FontAwesomeIcon icon={faGraduationCap} size="lg" />}
      >
        <h3 className="vertical-timeline-element-title">
          BS in Computer Science
        </h3>
        <h4 className="vertical-timeline-element-subtitle">
          University of South Florida
        </h4>
        <h4 className="vertical-timeline-element-subtitle">
          Tampa, FL
        </h4>
        <p>GPA: 3.66/4.0</p>
      </VerticalTimelineElement>
      <VerticalTimelineElement
        className="vertical-timeline-element--work"
        date="Summer 2016"
        iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
        icon={<FontAwesomeIcon icon={faBriefcase} size="lg" />}
      >
        <CardFlip
          first={
            <>
              <div>
                <h3 className="vertical-timeline-element-title">
                  Technology Summer Intern
                </h3>
                <h4 className="vertical-timeline-element-subtitle">
                  Citibank
                </h4>
                <h4 className="vertical-timeline-element-subtitle">
                  Tampa, FL
                </h4>
              </div>
              <p>
                SharePoint developer for internal sites
                <br />
                CSS | SharePoint
              </p>
            </>
          }
          second={
            <>
              <h3>Details</h3>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  Created wireframe/page layout for internal
                  SharePoint site
                </li>
                <li className="list-group-item">
                  Worked with team leads to improve each department’s
                  SharePoint site
                </li>
                <li className="list-group-item">
                  Modified webparts and CSS formatting to match styles
                  within parent sites
                </li>
              </ul>
            </>
          }
        />
      </VerticalTimelineElement>
    </VerticalTimeline>
  );
};
